
// Lists of words
var positiveWords = ['amor','felicidad','paz','compasión','óptimo','genuino','leal',
    'equitativo','valiente','caritativo','comprensivo','bueno','bonachón','altruista',
    'admirable','maravilloso','agradable','hermoso','compasivo','piadoso','sensible',
    'generoso','bondadoso','honesto','afectuoso','divertivo','tierno','simple','inocente',
    'beneficioso','conveniente','propicio','justo','excelente','generoso','desprendido',
    'dadivoso','caritativo','benéfico','abundante','productivo','noble','honroso','sublime',
    'desinteresado','sincero','leal','franco','digno','estimable','apreciable','sincero','claro',
    'espontáneo','natural','verdadero','real','cordial','honesto','decente','digno',
    'íntegro','sincero','justo','querido'];

var negativeWords = ['tristeza','preocupación','soledad','duda','angustia','soledad','negación',
    'malo','feo','horroroso','crítica','maldad','desconsuelo','abandono','golpear','agresión',
    'maltrato','injusticia','avaricia','apego','gritar'];
