//http://www.creandotuvida.com/Vocabulario_Positivo.html
var WordsPositive = {
    positiveWords: ['amor','felicidad','paz','compasión','óptimo','genuino','leal',
    'equitativo','valiente','caritativo','comprensivo','bueno','bonachón','altruista',
    'admirable','maravilloso','agradable','hermoso','compasivo','piadoso','sensible',
    'generoso','bondadoso','honesto','afectuoso','divertivo','tierno','simple','inocente',
    'beneficioso','conveniente','propicio','justo','excelente','generoso','desprendido',
    'dadivoso','caritativo','benéfico','abundante','productivo','noble','honroso','sublime',
    'desinteresado','sincero','leal','franco','digno','estimable','apreciable','sincero','claro',
    'espontáneo','natural','verdadero','real','cordial','honesto','decente','digno',
    'íntegro','sincero','justo','querido'],
    negativeWords: ['tristeza','preocupación','soledad','duda','angustia','soledad','negación',
    'malo','feo','horroroso','crítica','maldad','desconsuelo','abandono','golpear','agresión',
    'maltrato','injusticia','avaricia','apego','gritar'],
    totalPoints: 0,
    newPointsBar: 2,
    running: true,
    // Gets a new word
    getNewWord: function(){
        newWord = '';
        console.log("Getting a new word");
        // Possitive or negative
        posNeg = Math.floor(Math.random() * 10);
        if(posNeg < 5){
            console.log("Got a negative word: " + posNeg);
            wordPos = Math.floor(Math.random() * (this.negativeWords.length-1));
            newWord = this.negativeWords[wordPos];
            points = 0;
        }else{
            console.log("Got a positive word: " + posNeg);
            wordPos = Math.floor(Math.random() * (this.positiveWords.length-1));
            newWord = this.positiveWords[wordPos];
            points = 1;
        }
        console.log("Got word: " + newWord);
        //$(".wordsGame").last().remove();
        $("#wordsToView").html("<h1 onCLick='WordsPositive.sum("+points+");' class='wordsGame well' id='wordCurrent' style='text-align: center;'>"+newWord+"</h1>");

        // Set a timer in 1 second
        if(this.running === true){
        console.log("One more time...");
            setTimeout(function(){WordsPositive.getNewWord();}, 1000);
        }
    },
    // Sums points to the game
    sum: function(q){
        console.log("Adding points: " + q);
        if(q > 0){
            console.log("Got points");
            this.totalPoints = this.totalPoints + 1;
            $("#totalPoints").html(this.totalPoints);
            if(this.totalPoints >= this.newPointsBar){
                // Cala.Messages.danger("Muy bien!!!", 3);
                console.log("Goal reached");
                WordsPositive.pause(false);
                this.newPointsBar = this.newPointsBar * 2;
                sweetAlert({title: "Muy bien!", text: "La siguiente meta es " + this.newPointsBar + " puntos de felicidad", type: "success"},function(){
                    WordsPositive.start(); 
                });
                $("#totalPointsNextBar").html(this.newPointsBar);
            }
        }else{
            console.log("Adding more points for next target");
            this.newPointsBar = this.newPointsBar + 1;
            $("#totalPointsNextBar").html(this.newPointsBar);
        }
    },
    start: function(){
        console.log("Starting/resuming the game");
        this.running = true;
        setTimeout(function(){WordsPositive.getNewWord();}, 1000);
        $("#playGame").hide('slow', function(){$("#gameWords").show('slow');});
        //$("#gameInfo").hide('slow');
        //$("#gameWords").show('slow');
    },

    pause: function(full){
        console.log("Pausing the game...");
        this.running = false;
        if(full === true){
            $("#gameWords").hide('slow', function(){$("#playGame").show('slow');});
        }
    }
};

//setTimeout(function(){WordsPositive.getNewWord();}, 1000);
//WordsPositive.getNewWord();

