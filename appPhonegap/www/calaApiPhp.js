/****************************************************************************
/*!
 * Cala ApiPHP for Cala Framework: To make your life simpler
 * This file takes care of the users interface when running with Cala API (php)
 * as a backend.
 *
 * Copyright (c) 2015 Twisted Head
 * License: MIT
 *
 *           | |      
 *   ___ __ _| | __ _ 
 *  / __/ _` | |/ _` |
 * | (_| (_| | | (_| |
 *  \___\__,_|_|\__,_|
 *
 * version: 0.3.x-1
 *****************************************************************************/

var Users = Users || {};

var Users = {

    // Log users out, this is the function you should call
    logOut: function(){
        Cala.say("Logging out...");
        Users._logOut(
            function (){
                Cala.say("Successfully logged out");
                Users._logOutRmKeys();
            },
            function (){
                Cala.say("Error logging out with errors apparently");
                Users._logOutRmKeys();
            });

            return false; 
    },
    // Helper function to log users out, you should not call this directly
    _logOut: function(callMeSuccess, callMeError){

        Cala.say("Calling the remote log out");

        $.ajax({
            type: 'GET',
            url: Cala_apiUrl,
            dataType: "json",
            data: {
                r: "users_log_me_out",
                w: "users",
                iam: Users.getUserName(),
                sessionKey: Users.getSessionKey()
            },
            success: function (data) {
                callMeSuccess(data);
            },
            error: function (data){
                callMeError(ERROR_ERROR);
            }
        });
    },

    // Log users in
    logMeIn: function(){
        Cala.say("Logging in");
        Users._logMeIn({
            params: {
                userName: $("#myUserName").val(),
                pwd: $("#myPwd").val()
            },
            onSuccess: Users._logMeInSuccess,
            onError: Users._logMeInError
        });

        return false;

    },

    /**
     * What to do when the log in was a success
     * @todo Check that I was actually able to login!
     */
    _logMeInSuccess: function (details, redirect) {

        Cala.say("Able to login?");

        if (parseInt(details.resp.sessionKey) < 0) {
            Users._logMeInError();
        }

        else {
            Cala.Keys.store(VAR_CURRENT_USER_NAME, details.resp.userName);
            Cala.Keys.store(VAR_CURRENT_SESSION_KEY, details.resp.sessionKey);
            Users.getSessionKey();
            Users.getUserName();
            // Redirect only if nothing was said, this is like a default value of true
            if(redirect === undefined)
                Cala.Cala.iGoTo($('#goToAfterLogin').val());
        }

        return false;

    },

    /**
     *  Log users in
     */

    _logMeIn: function (details) {

        Cala.say("Loging in");

        $.ajax({
            type: 'GET',
            url: Cala_apiUrl,
            dataType: "json",
            data: {
                w: "users",
                r: "users_log_me_in",
                iam: '',
                userName: details.params.userName,
                pwd: details.params.pwd
            },
            success: function (data) {
                if (data.resp != ERROR_USER_WRONG_LOGIN_INFO) {
                    //Users._logMeInSuccess(data);
                    details.onSuccess(data);
                } else {
                    details.onError(ERROR_USER_WRONG_LOGIN_INFO);
                }
            },
            error: function (data) {
                details.onError(ERROR_ERROR);
            }
        });
    },

    _logMeInError: function(error) {
        Cala.say("Error trying to login:" + error);
        sweetAlert({title: "Error!", text: "Error intentando ingresar.\n", type: "error"});
    },

    // Auto login when running on embeded mode
    // @todo Confirm if this needs to be more secured
    autoLogin: function(){
        Cala.say("Should I auto login?");
        if(Cala.embeded === true){
            Cala.say("I am embeded");
            if(Cala.params.userName !== undefined && Cala.params.userName !== "" && Cala.params.sessionId !== undefined && Cala.params.sessionId !== ""){
                Cala.say("I got the params, I shall auto login");
                Users._logMeInSuccess({resp: {userName: Cala.params.userName, sessionKey: Cala.params.sessionId}}, false);
            }else{
                Cala.say("No new params, I will not auto login");
            }
        }
        return this;
    },

    // Get the name of the current user
    getUserName: function(){
        return Cala.Keys.get(VAR_CURRENT_USER_NAME, '');
    },

    // Get the current session key
    getSessionKey: function(){
        return Cala.Keys.get(VAR_CURRENT_SESSION_KEY, '');
    },

    // I'm I logged in?
    isLoggedIn: function(){

        Cala.say("Am I logged in?");

        if(Users.getUserName() === ''){
            Cala.say("No, I am not");
            return false;
        }else{
            Cala.say("Yes I am");
            return true;
        }
        //return this;
    },

    // Helper function to register a new user
    _userRegister: function(params) {

        Cala.say("Register a new account");

        $.ajax({
            type: 'GET',
            url: Cala_apiUrl,
            dataType: "json",
            data: {
                r: "users_register",
                w: "users",
                fullName: params.options.fullName,
                userName: params.options.userName,
                email: params.options.email,
                pwd: params.options.pwd,
                about: params.options.about,
                country: params.options.country,
                iam: '', 
                sessionKey: ''
            },

            success: function (data) {
                if (data.resp != ERROR_USER_EXISTS && data.resp != ERROR_BAD_REQUEST) {
                    Cala.say("Seems like it worked");
                    Users._logMeInSuccess(data);
                } else {
                    Cala.say("Something wrong happened");
                }
                params.onSuccess(data);

            },
            error: function (data) {
                params.onError(data);
            }
        });

    },

    // Helper function to get the details of the account
    _getMyDetails: function(callMeSuccess, callMeError) {

        Cala.say("Getting my details");

        $.ajax({
            type: 'GET',
            url: Cala_apiUrl,
            dataType: "json",
            data: {
                w: "users",
                r: "users_get_my_details",
                iam: Users.getUserName(),
                sessionKey: Users.getSessionKey()
            },
            success: function (data) {
                callMeSuccess(data);
            },
            error: function (data) {
                callMeError(ERROR_ERROR);
            }
        });

    },
    /**
     *  *  Get MY details, you need to be logged in in order for this to work
     *   */

    getMyDetails: function() {
        Cala.say("Getting my details");
        _usersGetMyDetails(
            function (data) {
                if (typeof data.resp == 'object') {
                    Cala.say("Got user details, at least it looks like it :)");
                    $("#myFullName").val(data.resp.fullName);
                    $("#myUserName").val(data.resp.userName);
                    $("#myEmail").val(data.resp.email);
                    $("#myAccountAbout").val(data.resp.about);
                }
                else {
                    Cala.say("Some error with the account details");
                }
            },
            function () {
                Cala.say("There was an error retrieving your information");
            });
    },
    /**
     * Update my account
     *
     */

    // Helper function to update the account
    _updateAccount: function(details) {

        Cala.say("Updating the user account");

        $.ajax({
            type: 'POST',
            url: Cala_apiUrl,
            dataType: "json",
            data: {
                w: "users",
                r: "users_update_profile",
                iam: Cala_IAM,
                sessionKey: Cala_SESSION_KEY,
                fullName: details.requestData.fullName,
                userName: details.requestData.userName,
                email: details.requestData.email,
                about: details.requestData.about,
                country: details.requestData.country,
                pwd: details.requestData.pwd
            },
            success: function (data) {
                Cala_IAM = details.requestData.userName;
                Cala.Keys.store(VAR_CURRENT_USER_NAME, details.requestData.userName);
                details.onSuccess(data);
            },
            error: function (data) {
                details.onError(ERROR_ERROR);
            }
        });

    },

    // Update a user account
    updateAccount: function() {

        Cala.say("Updating the user account...");

        Cala_usersUpdateAccount({
            requestData: {
                fullName: $("#myFullName").val(),
                userName: $("#myUserName").val(),
                email: $("#myEmail").val(),
                about: $("#myAccountAbout").val(),
                pwd: $("#myAccountPwd").val()
            },
            onSuccess: function (data) {
                if (data.resp == ERROR_NO_REQUEST_DONE) {
                    Tpl_msgWarning("Hubo un error actualizando sus datos, favor intente de nuevo mÃ¡s tarde");
                    Cala.say("Something happened");
                } else if (data.resp == ERROR_USER_EXISTS) {
                    Tpl_msgWarning("Los datos ya existen, intente con otro correo electrÃ³nico o nombre de usuario");
                }
                else {
                    Cala.say("Success updating profile");
                    Tpl_msgWarning("Sus datos fueron actualizados satisfactoriamente");
                    Cala.iGoTo("?x=myAccount");
                }
            },
            onError: function () {
                Cala.say("Error updating profile");
                Tpl_msgWarning("Hubo un error actualizando sus datos, favor intente de nuevo mÃ¡s tarde");
            }
        });
        return false;
    },

    // Helper function to remove all keys after log out
    _logOutRmKeys: function() {

        Cala.Messages.success("Hasta luego :)");

        keysGone = [VAR_CURRENT_USER_NAME, VAR_CURRENT_SESSION_KEY];

        // If I don't do it like this, it won't be able to remove them all
        for (i = 0; i < keysGone.length; i++) {
            Cala.Keys.remove(keysGone[i]);
        }

        Cala.Cala.iGoTo('index.html');

    },

    Tpl_userRegister: function() {

        Cala.say("Register a new user");

        if ($("#myPwdR").val() != $("#myPwdAgainR").val() || $("#myPwdR").val() === "") {
            Tpl_msgDanger("Las claves de acceso no coinciden");
            return false;
        }

        Users._userRegister({
            options: {
                fullName: $("#myFullNameR").val(),
                userName: $("#myUserNameR").val(),
                email: $("#myEmailR").val(),
                pwd: $("#myPwdR").val(),
                country: 'crc',
                about: ''},
                onSuccess: function (data) {
                    Cala.say("Success in registration request");
                    if (data.resp == ERROR_USER_EXISTS) {
                        Tpl_msgDanger("Este usuario ya existe");
                    }
                    else if (data.resp != ERROR_BAD_REQUEST) {
                        _logMeInSuccess(data);
                    }
                    else {
                        msgAlert("Sucedio un error, quizá hay campos pendientes");
                    }
                },
                onError: function () {
                    Cala.say("Error in registration request");
                    Tpl_msgDanger("SucediÃ³ un error, favor intentar nuevamente");
                }
        });

        return false;

    }

};

// Append it to Cala
jQuery.extend(Cala, Users);

