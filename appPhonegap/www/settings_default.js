/*******************************************************************************
 *
 * Cala
 *
 */

Cala.frontPage = "default";

// What device is this? Android|iOs|Comp
// Set the correct option for internal storage and other minor aspects
Cala.Device = 'comp';

// Should I debug with weinre? This will only apply for app installs
Cala.debugWeinre = false;

/*******************************************************************************
 *
 * jDrupal
 *
 */
// Set the site path (without the trailing slash). 
Drupal.settings.site_path = "http://0.0.0.0/server"; //http://www.example.com

// Set the Service Resource endpoint path.
Drupal.settings.endpoint = "rest";
