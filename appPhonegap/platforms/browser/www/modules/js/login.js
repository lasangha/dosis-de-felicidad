// Set the forms
$(function(){$('[data-toggle="tooltip"]').tooltip();})

function bhavana_FBLogin(){

    Cala.say("Logging into fb");
    openFB.login(
        function(response) {
            if(response.status === 'connected'){
                Cala.say('Facebook login succeeded, got access token: ' + response.authResponse.accessToken);
                openFB.api({
                    path: '/me?fields=email,name,id',
                    success: function(response) {
                        Cala.say(JSON.stringify(response));
                        Cala.login_fb(response.email, response.name, response.id);
                        //data.name
                        //document.getElementById("userPic").src = 'http://graph.facebook.com/' + data.id + '/picture?type=small';
                    },
                    error: function(){console.log("Error getting information about the user")}
                });
            }else{
                alert('Facebook login failed: ' + response.error);
            }
        }, {scope: 'email'});

}

// I should log you out of fb and then from the platform
function bhavana_fbLogout(){
    openFB.logout(
        function() {
            Cala.say('Logout successful from fb');
            Cala.logMeOut();
        },
        bhavana_errorHandler);
}

// This is just for tests, does nothing really
function bhavana_fbGetDets(){
    //@deprecated
    FB.api('/me?fields=id,name,email,permissions', function(response){
        Cala.say(JSON.stringify(response));
    });
}

// Special function to handle errors from the openFB thing
function bhavana_errorHandler(error) {
    console.log(error.message);
}

