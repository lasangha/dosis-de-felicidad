
var WordShuffle = {

    theWord: "",
    theWordArray: "",
    theWordShuffled: "",
    dificulty: 4,
    totalPoints: 0,

    //http://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
    shuffle: function(){
        this.theWordShuffled = this.theWordArray;
        var currentIndex = this.theWordShuffled.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = this.theWordShuffled[currentIndex];
            this.theWordShuffled[currentIndex] = this.theWordShuffled[randomIndex];
            this.theWordShuffled[randomIndex] = temporaryValue;
        }
        console.log("Shuffled word is: " + JSON.stringify(this.theWordShuffled));
        return this;
    },
    convertToArray: function(){
        console.log("Converting to array");
        this.theWordArray = this.theWord.split("");
        console.log("Final word is: " + JSON.stringify(this.theWordArray));
        return this;
    },
	_getTheWord: function(){
        console.log("Actually selecting a new word from the list");
        wordPos = Math.floor(Math.random() * (positiveWords.length-1));
		return positiveWords[wordPos]; 
	},
	getNewWord: function(){
		console.log("Selecting a new word from the list");

		this.theWord = "";

		// Is it too complicated
		while(this.theWord == ""){

			theWord = this._getTheWord();

			if(theWord.length > this.dificulty){
				console.log("Too long: " + theWord);
				theWord = "";
			}
			this.theWord = theWord;
		}
		console.log("WordPos: " + wordPos + " & the word: " + this.theWord);
		return this;
	},
	presentTheWord: function(){
		console.log("Presenting the word");
		$("#newWord").html(this.theWordShuffled.toString().replace(/,/g,'')).fadeIn('slow');
		$("#guessedWord").show('slow').focus();
		$("#viewWord").show('slow');
		return this;
	},
	checkGuessedWord: function(){
		guessedWord = $("#guessedWord").val();
		console.log("Checking the guessed word: " + guessedWord);
		if(guessedWord == this.theWord && guessedWord !== ""){
			// Add the points
			this.totalPoints = this.totalPoints + this.theWord.length;
			console.log("Total points now: " + this.totalPoints);
			console.log("Guessed it!");
			sweetAlert({title: "Muy bien!", text: "La palabra es: " + this.theWord + "\nPuntos totales: " + this.totalPoints, type: "success"},function(){
				WordShuffle.start(); 
			});
		}
	},
	viewWord: function(){
		console.log("Viewing the word");
		$("#newWord").html(this.theWord);
		$("#guessedWord").hide('slow');
		$("#viewWord").hide('slow');
	},
	start: function(full){
		console.log('Starting the game!!!');
		if(full === true){
			console.log("Hidding game info");
			$("#gameStart").hide('slow', function(){
				$("#gameCanvas").show('slow');
			});
		}
		// Some clean up
		$("#guessedWord").val("");
		$("#newWord").html("");
		WordShuffle.getNewWord().convertToArray().shuffle().presentTheWord();
	}
};
