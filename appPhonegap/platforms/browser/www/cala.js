/****************************************************************************
/*!
 * Cala Framework: To make your life simpler
 * @requires jQuery v1.5? or later, jDrupal|cala.js (maybe other things too)
 *
 * Copyright (c) 2015 Twisted Head
 * License: MIT

 * Include this AT THE BOTTOM of your pages, that is all you need to do.
 * Cala.boot();
 *           | |      
 *   ___ __ _| | __ _ 
 *  / __/ _` | |/ _` |
 * | (_| (_| | | (_| |
 *  \___\__,_|_|\__,_|
 *
 *****************************************************************************/                   
// Version
var version = "0.3.7";

//! The main instance of Cala
var Cala = Cala || {};

// This is just to add a sublevel to messages, use it if you want to
var subMsg = " --> ";

//! Internal useful things
VAR_GO_NOT_LOGGED_IN = '?x=login';

// Some other constants, this are used with Cala PHP api
var VAR_CURRENT_USER_NAME  = "current_user_name";
var VAR_CURRENT_SESSION_KEY = "current_session_key";
var VAR_CURRENT_USER_ID = "current_user_id";

// Users
var ERROR_NO_VALID_USER = "-300";
var ERROR_USER_WRONG_LOGIN_INFO = "-301";
var ERROR_USER_NO_VALID_SESSION = "-302"; // Not in use I think
var ERROR_USER_ACCESS_DENIED = "-303";
var ERROR_USER_EXISTS = "-304";

//Database
var ERROR_DB_NO_RESULTS_FOUND = "-200";
var ERROR_BAD_REQUEST = "-1";

/***************************************************************************
 *
 * I will try to use tha actual javascript way of using the cala object
 *
 */

var Cala = {

    //! Some variables
    // By default I think I run on a computer, this will change automatically
    onApp: false,
    // The front page when running on the default mode and on app
    frontPage: {def: 'bhavana/default', app: 'bhavana/default_app'},
    // Default path or this current path once it is set
    path: '',
    // Should I post tons of stuff in the console
    debug: false,
    // Debug in/with Weinre
    debugWeinre: false,
    // Run in embed mode?
    embed: false,
    // What device is this? Android|iOs|Comp
    // Set the correct option for internal storage and other minor aspects
    // @todo this will be autodetected; http://www.javascripter.net/faq/operatin.htm
    // @todo use this?
    device: 'comp',
    // Current IP, this will work only if you are running from the same server, but not in apps
    // So, I will assume that you are hosting the api in the same server, but change it if this
    // is not the case.
    ip: location.host,
    // All params posted via url
    params: [],
    // Stuff to run on boot
    runMe: [],
    // Stuff to run when connected to remote server
    runMeOnConnect: [],
    // All possible routes in the system
    routes:{
        'def':{
            title: 'Dark side of the moon',
            perms: 'none',
            internal: 'default'
        },
        'about':{
            title: 'About us',
            perms: 'none',
            internal: 'about'
        },
    },
    /**
     * Parse url parameters
     * http://jquery-howto.blogspot.com/2009/09/get-url-parameters-values-with-jquery.html
     */
    getUrlVars: function(){

        this.say("Loading all url vars (params)");
        // @todo This is not used anymore?
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++){
            hash = hashes[i].split('=');

            if(hash[1] !== undefined){
                // Fix 'errors' in params
                // @bug this is not the best solution
                var theKey = "%2F";
                var re = new RegExp(theKey, "g");
                hash[1] = hash[1].replace(re, "/");

                theKey = "%2f";
                re = new RegExp(theKey, "g");
                hash[1] = hash[1].replace(re, "/");
            }
            this.params.push(hash[0]);
            this.params[hash[0]] = hash[1];
            this.say(subMsg + "Found param: " + hash[0] + " >> " + hash[1]);
        }
        return true;
    },
    // Set the title of the page
    setPageTitle: function(newTitle){
        $("#pageFullTitle").html(newTitle);
    },
    // Run the onReady functions
    runOnReadyConnected: function (){
        for(i = 0; i < this.runMeOnConnect.length; i++){
            this.say("Running upon connect..." + i);
            this.justRunThis(this.runMeOnConnect[i]);
        }
        return this;
    },
    // Actually run functions, this is called internally only
    justRunThis: function(what){
        what();
        return this;
    },
    // Run when it is all ready, usually used by loaded pages
    runOnReady: function(what){
        $(document).ready(what);
    },
    // Get a param coming from the url or a default value
    paramsGet: function (which, def){
        if(this.params[which] !== undefined && this.params[which] !== ''){
            this.say("Found param with value: " + this.params[which]);
            return this.params[which];
        }
        else{
            this.say("No custom path");
            return def;
        }
    },
    // Say something
    say: function (what, type){
        if(this.debug === true){

            if(type === undefined){
                type = "m";
            }

            $("#debug").append(what + "<br />");
            if(type == "m"){
                //console.trace();
                console.log(what);
            }else if(type == "i"){
                console.info(what);
            }else if(type == "w"){
                console.warn(what);
            }else if(type == "e"){
                console.error(what);
            }
        }
    },
    /***************************************************************************
     *
     * Booting and start up
     *
     */

    /**
     * This is where it all begins, this should be run at the very beggining 
     * of the page load, or when document is ready, but you never call this directly,
     * you need to call whereAmI();
	 * @deprecated?
     */
    initMe: function(){

        this.say("Booting up the car!");

        // Get the params via url
        //this.getUrlVars();

        // Run things that people want me to run
        for(i = 0; i < this.runMe.length; i++){
            this.say("------>>>>>>Running... " + i, "i");
            this.justRunThis(this.runMe[i]);
        }
        return this;
    },
    // Connect to the main server
    // @bug when there is no connection I don't get an error, so I have no way of knowing if there is no connection
    connectToMainServer: function(onSuccess){

        this.say("Connecting to the main server...");

        this.say("Not running in embeded mode...");

        // I may be running with jDrupal or Cala API
        if(Cala_apiUrl === false){
            this.say("Running with jDrupal: " + Drupal.settings.site_path);
            // Perform a System Connect call.
            // @todo This should be core for the system to run
            system_connect({
                success:function(result){
                    if(Drupal.user.uid === 0){
                        message = "Hello World, Visitor!";
                    }else{
                        message = "Hello World, " + Drupal.user.name + "!";
                    }
                    Cala.say(message);
					// Run something on success
					if(onSuccess != undefined){
						onSuccess();
					}
                    //Cala.say("Hello: " + message);
                    // @bug if there is no connection, I am not sure if the user will be set,
                    // at the moment, if there is no connection -> I can't remember what I was saying :)
                    //this.runOnReadyConnected();
                    //Cala_finishBoot();
                },
                error:function(xhr, status, message){
                    //alert("?? 2");
                    Cala.info("Hubo un problema al conectarse al servidor, puede que algunas cosas no funcionen correctamente", 5);
                    //Cala_finishBoot();
                    //Cala.say("Error on system call, ther could be a problem with the internet or the main server");
                }
            });
        }else{
            this.say("Running with Cala Api");
            this.say("Connected to Cala Api...");
            if(Cala.isLoggedIn() === true){
                Cala.say("Apparently I am logged in, so, I shall verify the session");
                // Confirm the connection and the log in status
                $.ajax({
                    type: 'GET',
                    url: Cala_apiUrl,
                    dataType: "json",
                    data: {
                        r: "users_confirm_session_vilidity",
                        w: "users",
                        iam: Cala.getUserName(), 
                        sessionKey: Cala.getSessionKey()
                    },
                    success: function(data){
                        Cala.say("Got a response for the session");
                        if(data.resp == ERROR_USER_ACCESS_DENIED){
                            Cala.say("This session is not valid, logging out of here!");
                            Cala._logOutRmKeys(data);
                        }else{
                            Cala.say("Found a valid session");
                            //Cala_finishBoot();
                        }
                    },
                    error: function(data){
                        // This is not used at the moment
                        Cala.say("There was an error connecting to the server");
                    }
                });
            }else{
                //Cala_finishBoot();
            }
        }
        return this;
    },
    // Add things to run on boot, not using it a lot yet
    runOnBoot: function(what){
		Cala.say("Adding something to the run on boot");
        this.runMe.push(what);
    },
    //! Get the correct page for this path and load it
    loadThisPath: function(){

        this.say("Loading the current path");

        //this.workingOn.start("body");

        // Get the path
        path = this.paramsGet('x', this.getFrontPage());

        this.say(subMsg + "Path is: " + Cala_basePath + path);

        $("#mainContent").load("modules/" + path + ".html?ppp=" + Math.floor(Math.random() * 1000),
                               function(response, status){
                                   if(status == 'error'){
                                       Cala.say(subMsg + 'Path not found');
                                       sweetAlert({title: "Lo sentimos", text: "No hay nada en esta dirección", type: "error", timer: 2000},function(){
                                           Cala.iGoTo("index.html?src=app_404");
                                       });
                                   }
                                   $("#mainContent").fadeIn('slow');

                                   if(path != Cala.getFrontPage()){
                                       Cala.say(subMsg + "This is not the front page");

                                       // Add a back button
                                       $("#navBack").html('<i class="fa fa-chevron-left fa-1x"></i>');

                                       // Change the title
                                       // @todo The title could be set in a variable like this.pageTitle and load it from there
                                       var newTitle = $("#pageTitle").html();
                                       Cala.say("New title: " + newTitle);
                                       if(newTitle !== undefined){
                                           Cala.say(subMsg + "Changing the title");
                                           Cala.setPageTitle(newTitle);
                                       }
                                   }
                                   //Cala.workingOn.end("body");
                               }
                              );
                              return this;
    },

    /***************************************************************************
     *
     * Tools
     *
     */

    // Loads a script in the system
    // You may chain call it Cala.loadScript('one').loadScript('two')...
    loadScript: function(path, success){
        this.say("Loading a script: " + path);
        jQuery.getScript(path).done(success);
        return this;
    },

    // Loads the extra scripts for the app
    loadAppScripts: function(){
        this.say("Loading app scripts");
        $.getScript("cordova.js", function(data, textStatus, jqxhr){
            $.getScript("cordova.index.js", function(){
                Cala.say(subMsg + "Loaded cordova.index.js");
				Cala_finishBoot();
                // Add debugger
                if(Cala.debugWeinre !== false){
                    Cala.say(subMsg + "Loading weinre: " + Cala.debugWeinre);
                    $.getScript(Cala.debugWeinre + "/target/target-script-min.js#anonymous", function(){
                        Cala.say(subMsg + "Loaded weinre");
                        //Cala._boot();
                        app.initialize();
                    });
                }else{
                    Cala.say(subMsg + "Initializing app");
                    //Cala._boot();
                    app.initialize();
                }
            });
            Cala.say(subMsg + "Loaded cordova.js");
        });
    },
    // Gets the correct frontpage depending on the device
    // @todo add more devices or os
    getFrontPage: function(){

        if(this.onApp === true){
            return this.frontPage.app;
        }else{
            return this.frontPage.def;
        }
    },
    // Browser detection, this is only to automatically know if this is a browser or an app
    // @todo detect iOs|android|MS
    getProtocol: function(){
        protocol = document.location.protocol;
        this.say("Current protocol is: " + protocol);
        return this;
    },
	//@deprecated
    isThisAnApp: function(){
        this.say("Detecting protocol");
        if(document.location.protocol === 'file:'){
            this.say(subMsg + "Running on an app");
            this.onApp = true;
            // I need to first load ALL scripts and then I can boot up
            this.loadAppScripts();
        }else{
            this.say(subMsg + "Looks like a normal browser to me ?");
            this._boot();
        }
        return this;
    },

    // A new booting system, hopefully easier to follow!!!
    newBoot: function(){

        this.workingOn.start("body");

        this.say("Welcome my son, welcome to machine...!!", "i");

        // Get the url vars
        this.getUrlVars();

        this.say("Detecting protocol");

        if(document.location.protocol === 'file:'){
            this.say(subMsg + "Running on an app");
            this.onApp = true;
            // I need to first load ALL scripts and then I can boot up
            this.loadAppScripts(); // --> and this connectToMainServer()
        }else{
            this.say(subMsg + "Looks like a normal browser to me");
            // Connect to the main server
            //this.connectToMainServer();
            Cala_finishBoot();
        }
		//Cala.autoLogin();
        return this;

    },
    // This is what you should call from your pages, call it at the bottom
    boot: function(){

        this.workingOn.start("body");
        this.say("Welcome my son, welcome to machine...", "i");
    
        this.say("Detecting protocol");
        if(document.location.protocol === 'file:'){
            this.say(subMsg + "Running on an app");
            this.onApp = true;
            // I need to first load ALL scripts and then I can boot up
            this.loadAppScripts();
        }else{
            this.say(subMsg + "Looks like a normal browser to me");
            this._boot();
        }

        return this;
    
        //this.isThisAnApp();
        //Cala.Users.autoLogin();
    },
    // Finish the boot process
    endBoot: function(){
        this.say("Finished booting up...");
        this.workingOn.end("body");
    },
    // I will redirect somewhere
    iGoTo: function(goTo, msg){
        this.say("Going to: " + goTo, "i");
        // @bug sweetalert will re-pop when this function is called, 
        // this will prevent that new popup to be seen, it must be hidden fast
        // Store messages
        if(msg !== undefined){
            Cala.messagesStorePending(msg);
        }
        $("body").hide();
        window.location.href = goTo;
    },

    // Create an avatar with a default if needed
    avatarCreate: function(path){
        this.say("++++++++++++++++ : " + path);
        if(path == '0' || path === undefined){
            avatar = this.avatar;
        }else{
            avatar = path;
        }
        return avatar;
    },

    /***************************************************************************
     *
     * Templates and visual stuff
     * The rest of the templates are located in Cala.Tpl, this are a few functions that stayed here by mistake
     *
     */

    // Set a 'working' indicator
    workingOn: {
        // Start the indicator
        start: function(container){
            Cala.say("Working on...ini");
            $.smkProgressBar({
                element: container,
                status: 'start'
            });
        },
        // End the indicator
        end: function(container){
            Cala.say("Working on...end");
            $.smkProgressBar({
                element: container,
                status: 'end'
            });
        }
    },

    // 'Fix' the top menu with additional links and put the users name on it
    menuParse: function(){

        this.say("Am I logged in? Should I change the menu?");

        if(this.isLoggedIn() === true){
            this.say(subMsg + "Logged in, I will customize the menu");
            // The actual menu is located in an external file because this menu is usually customized for your
            // application, this approach makes it easier to upgrade the code.
            this._menuParse();
        }else{
            this.say(subMsg + "No, not logged in, no custom menu");
        }
        return this;
    },

    /***************************************************************************
     *
     * Messaging and alerts
     *
     */

    //! Clear the alert messages
    messagesClear: function(){
        $("#Cala_alertMessages").hide('slow', function(){
            $("#Cala_alertMessages").html('');
        });
        return this;
    },

    /**
     * Helper function to actually present messages to the user, you should never
     * call this directly
     */
    _alert: function(what, _type, t){

        this.say("Internal message" + t);

        var options = {text:what, type:_type};

        if(t === undefined){
            options.permanent = true;
        }else{
            options.time = t;
        }

        $.smkAlert(options);

        return this;
    },

    // Store messages for the next load
    messagesStorePending: function(msg){
        this.say("Storing the messages for the next load");
        Cala.Keys.store("pendingMessages", msg);
        return this;
    },

    // Load pending messages from the last page
    messagesLoadPending: function(){
        msgs = Cala.Keys.get("pendingMessages", false);
        if(msgs !== false){
            this.say("There are pending messages");
            this.success(msgs);
            Cala.Keys.remove("pendingMessages");
        }
        return this;
    },

    //! Alert messages to the user
    danger: function(what, t){
        this._alert(what, 'danger', t);
        return this;
    },

    // Alert warning messages to the user
    warning: function(what, t){
        this._alert(what, 'warning', t);
        return this;
    },

    // Alert information messages to the user
    info: function(what, t){
        this._alert(what, 'info', t);
        return this;
    },

    //! Success messages
    success: function(what, t){
        this._alert(what, 'success', t);
        return this;
    },

    /***************************************************************************
     *
     * Referals
     *
     */

    // Check and see if there is a referal link indicated
    referalCheckNew: function(){
        this.say("Checking for new referals");
        referal = this.paramsGet('referal', false);
        if(referal !== false){
            this.say("A referal was indicated");
            // Was there a previews referal stored?
            if(this.Keys.get("referal", "") === ""){
                this.say("There is no referal stored yet");
                this.Keys.store("referal", referal);
            }
        }else{
            this.say("No referal found");
        }
        return this;
    },
    // Get the currently set referal
    referalGetCurrent: function(){
        this.say("Is there a referal set?");
        referal = this.Keys.get("referal", "");
        if(this.Keys.get("referal", "") !== ""){
            this.say("Referal found :) -> " + referal);
            return referal;
        }else{
            this.say("No referal set");
        }
        return false;
    }
};

/***************************************************************************
 *
 * Local storage
 *
 */

//! Store keys in local storage
Cala.Keys = {
    // Store keys
    store: function(key, value){
        Cala.say("Storing key: " + key + " >> " + value);
        window.localStorage.setItem(key, value);
        return true;
    },

    //! Get key from local storage
    get: function(key, defaultValue){
        Cala.say("Loading key: " + key);
        var value = window.localStorage.getItem(key);
        if(value === null){
            Cala.say(subMsg + "No value found, I will use the default" + defaultValue);
            value = defaultValue;
        }
        Cala.say(subMsg + "Gotten Key: " + key + " with value: " + value);
        return value;
    },

    //! Remove key from local storage
    remove: function(theKey){

        Cala.say("Removing key: " + theKey);
        // Remove them all
        if(theKey === ''){
            Cala.say(subMsg + "Removing all keys");
            localStorage.clear();
        }else{
            window.localStorage.removeItem(theKey);
        }
    },
};

/***************************************************************************
 *
 * Templates
 *
 */

Cala.Tpl = {

    //! Add a css
    addCss: function (which){

        Cala.say("Adding a css: " + which);
        // Is this an external css
        if(strpos(which, 'http', 0) === false){
            Cala.say(" ->> Its an internal file");
            which = "modules/" + which;
        }

        $('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', which));
    },
    // Hide/Display the top menu
    displayTopMenu: function (){
        Cala.say("Showing/hidding top menu");
        $('#Cala_topMenu').toggle('slow');
        window.scrollTo(0,0);
        return false;
    }

};

/**
 *  I parse a tpl and replace it with some values
 *  @param tpl The template
 *  @param values An object with the values to replace
 *  @param bl Do you want me to replace the breaklines (\n) with <br />
 */
function parseTpl(tpl, values, bl){

    // Loop each value
    for (var key in values) {
        var theKey = "{" + key + "}";
        var re = new RegExp(theKey, "g");
        tpl = tpl.replace(re, values[key]);
    }

    // Should I add html breakLines?
    if(bl === true){
        tpl = textAddBreakLines(tpl);
    }

    return tpl;

}

// Parse a date from a UTC timestamp
// Read more https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Text_formatting
function dateParse(timestamp){

    //var utcSeconds = 1234567890;
    var d = new Date(0);
    d.setUTCSeconds(timestamp);
    timestamp = d.toString();

    return timestamp;

}

/**
 * String poss of a word/string in a string
 * http://phpjs.org/functions/strpos/
 * @param haystack where are you looking in?
 * @param needle what are you looking for?
 * @param offset where do you want to start the search?
 */
function strpos(haystack, needle, offset) {
    //  discuss at: http://phpjs.org/functions/strpos/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Onno Marsman
    // improved by: Brett Zamir (http://brett-zamir.me)
    // bugfixed by: Daniel Esteban
    //   example 1: strpos('Kevin van Zonneveld', 'e', 5);
    //   returns 1: 14

    var i = (haystack + '')
    .indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
}

// Add html break lines to the text
function textAddBreakLines(text){
    var theKey = "\n";
    var re = new RegExp(theKey, "g");
    text = text.replace(re, "<br />");
    return text;
}

/**
 * Opens external links in browser or in an external brower if running on phonegap
 */
function Cala_externalLink(linkId){

    // I will only act if I am running on an app
    if(Cala.onApp === false){
        return true;
    }else{
        // Get the path
        var whereTo = $("#" + linkId).attr("href");
        if(whereTo.indexOf("http") === 0){
            Cala.say("External link");
        }else{
            whereTo = "http://meditacion.lasangha.org/" + whereTo;
        }
        Cala.say("Clicked me: " + whereTo);
        navigator.app.loadUrl(whereTo, { openExternal:true });
        return false; 
    }
}

/***************************************************************************
 *
 * User Management
 *
 */

// Sets an alert when the user is not logged in and goes to the login page
function Cala_notLoggedInAlert(){
    sweetAlert({
        title: "Error!",
        text: "Requiere una cuenta para poder utilizar estas funciones.\n",
        type: "error",
        timer: 5000
    },function(){
        Cala.iGoTo("?x=login&src=app_notLoggedIn");
    });

}

// Confirms that the user is logged in
// You may set an additional message in case the user is not
// Anyone can pretend to be logged in, this will just redirect,
// but it is not very secure to depend on this for security
// @todo add a function to call on success
// not really working yet
// @deprecated It seems to be in used
function Cala_confirmLoggedIn(){
    Cala.say("I need to be logged in to see this page");
    //Cala.runMeOnConnect.push(
    Cala.isLoggedIn({
        onSuccess: function(){
            Cala.say("I am logged in, I can see this page");
            return this;
        },
        onError: function(){
            Cala.say("I am NOT logged in, I should not be able to see this page");
            Cala_notLoggedInAlert();
        }});
        //);
}

/**
 * Parse error messages sent by drupal
 */
function Cala_parseErrorMessagesDrupal(result){

    // Get the response text
    errorMessages = JSON.parse(result.responseText);

    // The full error message(s)
    fullErrorMessage = "";

    if(errorMessages.form_errors !== undefined){

        Cala.say("Getting form_errors");

        // Get all error messages
        formErrors = errorMessages.form_errors;

        formErrorsKeys = Object.keys(formErrors);

        // Iterate and store them in a single error message
        for (var i = 0; i < formErrorsKeys.length; i++) {
            Cala.say("Adding one error message: " + i);
            fullErrorMessage = fullErrorMessage + formErrors[formErrorsKeys[i]];
        }
    }else{
        fullErrorMessage = errorMessages;
    }

    return fullErrorMessage;

}

/***************************************************************************
 *
 * App
 *
 */

/**
 * Creates a path for the audios depending if it is a web version or an app
 */
function Cala_createAudioPath(){
    if(Cala.onApp === true){
        return Cala_getPhoneGapPath();
    }else{
        return '';
    }
}

/**
 * The the current path for phonegap, this allows the plugin to work with the app server
 * http://stackoverflow.com/questions/4438822/playing-local-sound-in-phonegap
 */
function Cala_getPhoneGapPath() {

    Cala.say("Detecting phonegap path");
    var path = window.location.pathname;
    path = path.substr( path, path.length - 10 );
    Cala.say("The phone path is: " + path);
    return 'file://' + path;

}

// @deprecated ?
//$('#Cala_topMenu').toggle(); // This should happen on each page load

// Set a message in a chart if no information was found
// @todo Does this belong in here?
function Cala_chartsNoInfo(chartId, extraMessage){

    Cala.say("No information found for this chart: " + chartId);

    // Add a breakline if required
    if(extraMessage !== ""){
        extraMessage = "<br />"+extraMessage;
    }

    //$("#"+chartId).html("No hay información en este momento<br />" + extraMessage);
    $("#"+chartId).html("<div role='alert' class='alert alert-warning'>No hay resultados en este momento." + extraMessage + "</div>");

}

/***************************************************************************
 *
 * Screen on and off
 * Keep it on, this is very specific, this should not even be here
 *
 */

// Enable
function Cala_keepScreenOn(){
    // @todo set this on only if the timer is running
  	if(Cala.onApp === true){
        Cala.say("Keeping the screen on");
        keepscreenon.enable();
    }
}

// Go back to normal
// @bug this is not really working, at least not in android
function Cala_keepScreenOff(){
    if(Cala.onApp === true){
        Cala.say("Keeping the screen off");
        keepscreenon.disable();
    }
}

// Call this if you are running as an embeded application
function Cala_embeded(){
    // Then I should recieve a session
}

// ** Test of new boot ** //
// I had to put this 2 function out here in order to avoid problems with the Cala object and
// with the syncronicity (how ever you say that word)
// @deprecated
function Cala_boot(){
    $(document).ready(function(){
        Cala.say("Booting cala...");
        // This last call will finish the booting process when connected
        //Cala.boot().isThisAnApp().autoLogin().connectToMainServer();
        //Cala.boot().autoLogin().connectToMainServer();
        Cala.newBoot();
    });
}

// Finish the booting process
function Cala_finishBoot(){
    //Cala.menuParse().messagesLoadPending().loadThisPath().referalCheckNew().endBoot();
    //Cala.autoLoginFake().menuParse().messagesLoadPending().loadThisPath().endBoot();
    Cala.messagesLoadPending().loadThisPath().endBoot();
}

/* Some extra functions that I am testing */
function Cala_addConnectToMainServer(){
	Cala.runOnReady(function(){Cala.connectToMainServer()});
}


/* TMP - new ajax calls */
function cala_ajaxCall(){
	// Obtain session token.
	$.ajax({
		url: Drupal.settings.site_path + "?q=services/session/token",
	type:"get",
	dataType:"text",
	error:function (jqXHR, textStatus, errorThrown) {
		alert(errorThrown);
	},
	success: function (token) {
		console.log("-->>" + token);
		// Call system connect with session token.
		$.ajax({
			url: Drupal.settings.site_path + '?q=rest/bhavana_resources/badges_parse.json',
			type: "post",
			dataType: "json",
			data: JSON.stringify({which: 'meditation'}),
			//data: 'username=ananda&password=13VitaPto_an',
			error: function (jqXHR, textStatus, errorThrown) {
				Cala.say(errorThrown);
			},
			success: function (data) {
				Cala.say("Got it");
			}
		});
	}
	});
}

// Loads iframes and displays a wating visual information
function Cala_loadFrame(iframeId, iframeSrc, w, h){

	$("#" + iframeId).html('<div id="cala_frameLoading" class="alert alert-info" type="alert">Cargando... <img src="modules/bhavana/img/loader_balls.gif" /></div>' +
			'<iframe id="iframe_' + iframeId + '" src="' + iframeSrc + '" width="' + w + '" height="' + h + '" frameBorder="0"></iframe>');

	$('#iframe_' + iframeId).on("load", function() {
			$('#cala_frameLoading').hide('slow');
			$("#" + iframeId).show('slow');
			});
	return true;
}

