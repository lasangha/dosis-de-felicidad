/****************************************************************************
/*!
 * Cala jDrupal for Cala Framework: To make your life simpler
 * This file takes care of the users interface when running with Drupal as a backend.
 * @requires Cala Framework LATEST! always
 *
 * Copyright (c) 2015 Twisted Head
 * License: MIT
 *
 *           | |      
 *   ___ __ _| | __ _ 
 *  / __/ _` | |/ _` |
 * | (_| (_| | | (_| |
 *  \___\__,_|_|\__,_|
 *
 * version: 0.3.x-1
 *****************************************************************************/
var Users = Users || {};

var Users = {
    // Auto login when running on embeded mode
    // @todo Test this out, I have not done it
    autoLogin:function(){
        Cala.say("Should I auto login?");
        if(Cala.params.userName !== undefined && Cala.params.userName !== "" && Cala.params.sessionId !== undefined && Cala.params.sessionId !== ""){
            Cala.say("I got the params, I shall auto login");
            Drupal.user.name = params.userName;
            Drupal.user.uid  = params.idUser;
        }
        return this;
    },
    getUserName: function(){
        return Drupal.user.name;
    },
    // Log a user in
    logMeIn: function(){
        Cala.say("Loging you in...");
        userName = $("#myUserName").val();
        password = $("#myPwd").val();
        if(userName === "" || password  === ""){
            Cala.say("Something is missing");
            Cala.messagesClear().warning("Faltan algunos campos");
            $('#loginButton').button('reset');
            return false;
        }

        this._logMeIn(userName, password);

        return false;
    },

    // Helper function to actually log someone in
    _logMeIn: function(userName, userPassword, options){

        Cala.workingOn.start("body");

        user_login(userName, userPassword, {
            success:function(result){
                sweetAlert({title: "Listo!", text: "Hola de nuevo " + result.user.name, type: "success", timer: 2000},function(){
                    Cala.iGoTo("index.html?src=app_login");
                });
                if(options.onSuccess){
                    error();
                }
            },
            error: function(xhr, status, message){
                Cala.messagesClear().warning("Datos incorrectos, ¿Quizá olvidó su clave?");
                Cala.say("Unable to login");
                Cala.workingOn.end();
                if(options.onError){
                    error();
                }
            }
        });

        //Cala.workingOn.end("body");
    },
    // Is the user logged in?
    isLoggedIn: function(data){
        Cala.say("Am I logged in?");
        if(Drupal.user.uid > 0){
            Cala.say("I am logged in, I can see this page");
            if(data !== undefined)
                data.onSuccess();
            return true;
        }else{
            Cala.say("I am NOT logged in");
            if(data !== undefined)
                data.onError();
            return false;
        }
        //return this;
    },
    // Helper function to log in the user
    // @deprecated? This does not do much in here, not in this version
    logMeInSuccess: function(data){
        //Cala.Keys.store(VAR_CURRENT_USER_NAME, data.resp.userName);
        //Cala.Keys.store(VAR_CURRENT_SESSION_KEY, data.resp.sessionKey);
        //Cala_SESSION_KEY = data.resp.sessionKey;
        return this;
    },
    // Log a user out
    logMeOut: function(){

        Cala.workingOn.start("body");

        user_logout({
            success:function(result){
                if (result[0]) {
                    Cala.say("I am logged out, just saying goodbye...");
                    // Empty the local storage
                    //calStorage.clear();
                    sweetAlert({title: "Hasta luego!", text: "Recuerde estar bien siempre :)", type: "success", timer: 2000},function(){
                        Cala.iGoTo("index.html?src=app_logOut");
                    });
                }
            }
        });

        Cala.workingOn.end("body");

        return this;
    },
    // Request a new password
    passwordReset: function(){

        userName = $("#myRecoverEmail").val();

        if(userName === ""){
            Cala.say("Something is missing");
            Cala.messagesClear().warning("Requiero al menos un usuario para este procedimiento");
            return false;
        }

        user_request_new_password(userName, {
            success: function(result) {
                if (result[0]) {
                    Cala.say("All good, a new password in underway");
                    Cala.messagesClear().success("Las instrucciones para terminar con el proceso serán enviadas a su correo electrónico");
                }
            },
            error: function(){
                Cala.messagesClear().danger("Hubo un error, quizá el correo no existe. Favor intente de nuevo.");
            }
        });
        return false;
    },
    // Register a new user
    registerNew: function(){

        userEmail = $("#myEmailR").val();
        password = $("#myPwdR").val();

        if(userEmail === "" || password  === ""){
            Cala.say("Something is missing");
            Cala.messagesClear().warning("Faltan algunos campos");
            return false;
        }

        var account = {
            name: userEmail,
            mail: userEmail,
            pass: password
        };

        user_register(account, {
            success: function(result) {
                Cala.say('Registered user #' + result.uid);
                Cala._logMeIn(userEmail, password);
            },
            error: function(result){
                Cala.messagesClear().warning("Hubo un error, quizá ya este correo existe, si este es su correo y olvidó la clave, puede solicitar una nueva. ¿Quizá la clave no coincide?");
            }
        });

        return false;

    },

    // Login via fb
    login_fb: function(userEmail, userName, userPassword){

        var account = {
            name: userName,
            mail: userEmail,
            pass: userPassword
        };

        user_register(account, {
            success: function(result) {
                Cala.say('Registered user #' + result.uid);
                Cala._logMeIn(userEmail, userPassword);
            },
            // An error in this case means that the persons already has an account, if the initial set up was NOT via fb
            // then this will not work, but if it was, then this should work fine.
            error: function(result){
                Cala.say("Account exists, I will try to login.");
                Cala._logMeIn(userEmail, userPassword, {onError: function(){bhavana_fbLogout()}});
            }
        });
    },
    // Set details when visiting the account
    setEditAccount: function(){
        Cala.say("Visiting my account: " + Drupal.user.uid);
        user_load(Drupal.user.uid, {
            success:function(account) {
                $("#myUserName").val(account.name);
                $("#myEmail").val(account.mail);
                Cala.say("Loaded user " + account.name);
            }
        });
    },
    // Update account details
    updateAccount: function(){

        Cala.workingOn.start("body");

        Cala.say("Updating account");

        var account = {
            uid: Drupal.user.uid,
            name: $("#myUserName").val(),
            mail: $("#myEmail").val(),
            current_pass: $("#myAccountCurrentPwd").val()
        };

        if($("#myAccountPwd").val() !== ""){
            Cala.say("Using a new password: " + $("#myAccountPwd").val());
            account.pass = $("#myAccountPwd").val();
        }

        user_update(account, {
            success:function(result) {
                Cala.say('Updated user #' + result.uid);
                Cala.workingOn.end("body");
                sweetAlert({title: "Listo!", text: "Los cambios fueron hechos", type: "success", timer: 2000}, function(){
                    Cala.iGoTo("?x=myAccount&src=app_updateProfile");
                });
            },
            error:function(result){
                Cala.messagesClear().warning(Cala_parseErrorMessagesDrupal(result));
                Cala.workingOn.end("body");
            }
        });
    },
    // Confirms that the user is logged in
    // You may set an additional message in case the user is not
    // @todo add a function to call on success
    // @deprecated Moved to the new js, or it should be
    confirmLoggedIn: function(onSuccess){
        Cala.say("I need to be logged in to see this page");
        if(Drupal.user.uid > 0){
            Cala.say("I am logged in, I can see this page");
            onSuccess();
            return true;
        }else{
            Cala.say("I am NOT logged in");
            Cala_notLoggedInAlert("");
            return false;
        }
    },

};

// Append it to Cala
jQuery.extend(Cala, Users);
